/*! ----------------------------------------------------------------------------
 * @file	port.h
 * @brief	HW specific definitions and functions for portability
 *
 * @attention
 *
 * Copyright 2013 (c) DecaWave Ltd, Dublin, Ireland.
 *
 * All rights reserved.
 *
 * @author DecaWave
 */


#ifndef PORT_H_
#define PORT_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f4xx_hal.h"
#include "usart.h"
#include "tim.h"

/* Define our wanted value of CLOCKS_PER_SEC so that we have a millisecond
 * tick timer. */
#undef CLOCKS_PER_SEC
#define CLOCKS_PER_SEC 1000

//void printf2(const char *format, ...);

extern int writetospi_serial(uint16_t headerLength,
                             const uint8_t *headerBuffer,
                             uint32_t bodylength,
                             const uint8_t *bodyBuffer);

extern int readfromspi_serial(uint16_t	headerLength,
                              const uint8_t *headerBuffer,
                              uint32_t readlength,
                              uint8_t *readBuffer );

#define writetospi  writetospi_serial
#define readfromspi readfromspi_serial
#define UWB_TIM_Init MX_TIM1_Init
#define UWB_TIMTER  TIM1
#define UWB_htim htim1
#define UWB_USART huart1
															//！！！！重要说明！！！！
//天线延时校正，如果测得距离和实际距离有稳定偏差，可以适当调整这个参数修正
//参考视频:https://www.bilibili.com/video/BV1154y1q7Qp/
#ifndef RX_ANT_DLY
#define RX_ANT_DLY 32900
#endif
//TX 天线延时，设置为0即可，只用RX 天线延时即可
#ifndef TX_ANT_DLY
#define TX_ANT_DLY 0
#endif
#ifdef __cplusplus
}
#endif

#endif /* PORT_H_ */
