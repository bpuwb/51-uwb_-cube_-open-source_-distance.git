#ifndef BPHERO_UWB_H
#define BPHERO_UWB_H

#include "frame_header.h"
#include "common_header.h"

//！！！！重要说明！！！！
//RX_NODE 和 TX_NODE 编译的时候只能有且只有一个打开
//RX_NODE  接收节点，通常称作为基站
//TX_NODE  发送节点，通常称作为标签
//#define RX_NODE
#define TX_NODE


//如果是RX节点，定义短地址为0x0002
#ifdef RX_NODE
#define SHORT_ADDR 0x0002
#endif

//如果是TX节点，定义短地址为0x0001并且使能LCD
#ifdef TX_NODE
#define SHORT_ADDR 0x0001
//#define LCD_ENABLE
#endif

//网络ID，如果同一个区域有多套设备存在，可以修改这个参数，每一套一个虚拟网络ID
#define NET_PANID 0xF0F2

extern int psduLength ;
extern srd_msg_dsss msg_f_send ; // ranging message frame with 16-bit addresses

//定义光速
#ifndef SPEED_OF_LIGHT
#define SPEED_OF_LIGHT      (299702547.0)     // in m/s in air
#endif

//定义UWB数据包长度
#ifndef FRAME_LEN_MAX
#define FRAME_LEN_MAX 127
#endif

//发送Final信息用到的偏移定义
#define FINAL_MSG_POLL_TX_TS_IDX 2
#define FINAL_MSG_RESP_RX_TS_IDX 6
#define FINAL_MSG_FINAL_TX_TS_IDX 10
#define FINAL_MSG_TS_LEN 4

/* UWB microsecond (uus) to device time unit (dtu, around 15.65 ps) conversion factor.
 * 1 uus = 512 / 499.2 ? and 1 ? = 499.2 * 128 dtu. */
#define UUS_TO_DWT_TIME 65536

///* Receive response timeout */
#define RESP_RX_TIMEOUT_UUS 5700
extern uint8 rx_buffer[FRAME_LEN_MAX];
extern uint32 status_reg;
extern uint16 frame_len ;
extern void BPhero_UWB_Message_Init(void);
extern void BPhero_UWB_Init(void);//dwm1000 init related
extern float dwGetReceivePower(void);

#endif
