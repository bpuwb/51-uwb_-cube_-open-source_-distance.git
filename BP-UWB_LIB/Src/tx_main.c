/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include "port.h"
#include "deca_device_api.h"
#include "deca_regs.h"
#include "port.h"
#include "frame_header.h"
#include "common_header.h"
#include "bphero_uwb.h"
#include "dwm1000_timestamp.h"
#include "kalman.h"
#include "tim.h"
#include "main.h"
#include "bphero_uwb.h"
#ifdef LCD_ENABLE
	#include "lcd_oled.h"
#endif
static unsigned char distance_seqnum = 0;
static srd_msg_dsss *msg_f_recv ;
/******************************************************************************************************************/
//定义TAG 两种状态
#define TAG_INIT            0
#define TAG_POLL_SENT   		1
//设定TAG默认状态为INIT
char Tag_State = TAG_INIT;
/*******************************************************************************************************************/
//TWR过程中用到的6个时间戳，RX TX NODE一共用到6个，这里用static定义，放置和rx_main.c冲突
//！！！TWR 定位原理说明，请参见51uwb.cn视频讲解！！！
//视频1 ： https://www.bilibili.com/video/BV1jt4y1y7v8/
//视频2 ： https://www.bilibili.com/video/BV1GT4y1J7Ky/

//static uint64 poll_rx_ts;
//static uint64 resp_tx_ts;
//static uint64 final_rx_ts;
 
static uint64 poll_tx_ts;
static uint64 resp_rx_ts;
static uint64 final_tx_ts;
/*******************************************************************************************************************/
 #ifdef LCD_ENABLE
	char dist_str[16] = {0};
#endif
/***************TX 节点 UWB中断处理函数**************/
void Tx_Simple_Rx_Callback()
{
    uint32 status_reg = 0;
    uint32 final_tx_time;
    int Final_Distance = 0;//保存计算出的实际距离

    //每次进入接收中断函数，先清理缓存区域，准备接收数据
    for (uint32 i = 0 ; i < FRAME_LEN_MAX; i++ )
    {
        rx_buffer[i] = '\0';
    }
    //通过帧过滤功能，配置UWB不接收任何数据，相当于关闭接收
    dwt_enableframefilter(DWT_FF_NOTYPE_EN);//disable recevie
    //读取UWB状态寄存器
    status_reg = dwt_read32bitreg(SYS_STATUS_ID);
    //判断是否有完整数据接收完毕
    if (status_reg & SYS_STATUS_RXFCG)
    {
        //读取接收到的数据长度
        frame_len = dwt_read32bitreg(RX_FINFO_ID) & RX_FINFO_RXFL_MASK_1023;
        //如果数据长度小于最大值，认为是合理的
        if (frame_len <= FRAME_LEN_MAX)
        {
            //读取接收到的数据
            dwt_readrxdata(rx_buffer, frame_len, 0);
            //将数据强制转换成约定格式
            msg_f_recv = (srd_msg_dsss*)rx_buffer;
            //提取发送该数据的短地址，并赋值到将要发送信息的目标地址
            //哪里来的信息，后面回复给谁
            msg_f_send.destAddr[0] = msg_f_recv->sourceAddr[0];
            msg_f_send.destAddr[1] = msg_f_recv->sourceAddr[1];
            //提取sequence Num
            msg_f_send.seqNum = msg_f_recv->seqNum;
            //因为我们TWR交互只用到第一个数据，所以这里只判断第一个数据就可以
            switch(msg_f_recv->messageData[0])
            {
            case 'A'://字符A是rx节点回复tx节点POLL信息
                if(Tag_State == TAG_POLL_SENT)//tx节点的当前状态是TAG_POLL_SENT
                {
                    //这部分是TWR原理比较难理解的地方，建议参考我们视频，更多视频内容参见www.51uwb.cn
                    //读取上次发送的时间戳
                    poll_tx_ts = get_tx_timestamp_u64();
                    //读取这次接收的时间戳
                    resp_rx_ts = get_rx_timestamp_u64();
                    //使用delayed tx方式发送当前时间戳
                    final_tx_time =   dwt_readsystimestamphi32()  + 0x17cdc00/60;//10ms/6
                    //final_tx_time =   dwt_readsystimestamphi32()  + 0x17cdc00/90; fail!!
                    dwt_setdelayedtrxtime(final_tx_time);
                    final_tx_ts = (((uint64)(final_tx_time & 0xFFFFFFFE)) << 8);
                    //配置数据包有效载荷，这里只有单个字符'F'
                    msg_f_send.messageData[0]='F';//Final message
                    final_msg_set_ts(&msg_f_send.messageData[FINAL_MSG_POLL_TX_TS_IDX], poll_tx_ts);
                    final_msg_set_ts(&msg_f_send.messageData[FINAL_MSG_RESP_RX_TS_IDX], resp_rx_ts);
                    final_msg_set_ts(&msg_f_send.messageData[FINAL_MSG_FINAL_TX_TS_IDX], final_tx_ts);
                    dwt_writetxdata(psduLength + 14, (uint8 *)&msg_f_send, 0) ; // write the frame data
                    dwt_writetxfctrl(psduLength +14, 0);
                    //Delayed 模式发送数据
                    dwt_starttx(DWT_START_TX_DELAYED);
                    //等待发送完成
                    while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
                    { };
                    //清楚rx和 tx 标志
                    dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_RXFCG | SYS_STATUS_TXFRS);

                    //这里注意！！！
                    //当收到rx节点发送的'A'信息，也会将上次的测距距离信息发送过来
                    //根据约定的数据格式解包并通过串口打印和在LCD显示(如果LCD使能)
                    Final_Distance = (msg_f_recv->messageData[1]*100 + msg_f_recv->messageData[2]);//cm
                    printf("0x%04X <--> 0x%02X%02X :%d cm\n",SHORT_ADDR,msg_f_send.destAddr[1],msg_f_send.destAddr[0],(int)((Final_Distance)));
#ifdef LCD_ENABLE
										sprintf(dist_str, "  Dis: %.2f m", (float)Final_Distance/100);
                    OLED_ShowString(0, 4,dist_str);
#endif
                    float uwb_rssi = 0;
                    uwb_rssi = dwGetReceivePower();
                    printf("RSSI:%2.2fdB ",uwb_rssi);
#ifdef LCD_ENABLE
										sprintf(dist_str, " RSSI: %2.2fdB", uwb_rssi);
                    OLED_ShowString(0, 6,dist_str);
#endif
                    Tag_State = TAG_INIT;
                }
                break;

            default:
                break;
            }
        }
    }
    else
    {
        //如果是异常数据，清理各种接收标识
        dwt_write32bitreg(SYS_STATUS_ID, (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_ERR));
        //重新设定标签状态为INIT状态
        Tag_State = TAG_INIT;
    }
}
/***************启动和特定地址进行测距**************/
void BPhero_Distance_Measure_Specail_TAG(void)
{
    //设定对方的地址，这里使用的地址是自身地址+1.
    //这个地址也可以修改，但是需要注意编译RX节点的时候地址需要与这个目标地址一致，否则无法完成测距
    msg_f_send.destAddr[0] =(SHORT_ADDR+1) &0xFF;
    msg_f_send.destAddr[1] =  ((SHORT_ADDR+1)>>8) &0xFF;
    //配置一个全局的sequence num
    msg_f_send.seqNum = distance_seqnum;
    //配置数据包有效载荷，这里只有单个字符'P'
    //如果要实现自定义传输额外数据，可以追加到messageData 数组中，并且在发送的时候重新计算长度
    msg_f_send.messageData[0]='P';//Poll message
    //将要发送的数据写入到UWB寄存器内
	  //  STM32L151只要使用psduLength 就有问题，需要debug原因！！！！！ 
    dwt_writetxdata(psduLength+1,(uint8 *)&msg_f_send, 0) ;  // write the frame data
    //告知UWB发送数据偏移为0
    dwt_writetxfctrl(psduLength+ 1, 0);
    //启动立即发送
    dwt_starttx(DWT_START_TX_IMMEDIATE);
    //等待发送完成
    while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
    { };
    //启动接收
    //Step1:启动帧过滤功能 --> 只接收数据包，更多帧过滤功能相关内容可以参考51uwb.cn
    dwt_enableframefilter(DWT_FF_DATA_EN);
    //Step2:设定接收延时，如果发送完，接收节点没有回复，自动退出接收模块
    //如果不设置这个参数，uwb一致处于接收模式，接收模式是uwb工作状态中最费电的模式，尽量避免长时间无用等待
    dwt_setrxtimeout(RESP_RX_TIMEOUT_UUS*10);
    //Step3:立马启动接收，这个函数里的参数可以设置延时接收，可以优化，让接收机过段时间启动，减少能量损耗
    dwt_rxenable(0);

    //对Sequence Num进行自增
    if(++distance_seqnum == 255)
        distance_seqnum = 0;
    //更改标签状态为 "POLL信息已发"
    Tag_State = TAG_POLL_SENT;
}
/************************************************************************/
/************************************************************************/
/************************使用定时器周期性启动测距************************/
/********************************BEGIN***********************************/
/************************************************************************/

#ifdef TX_NODE
/* 定时器中断回调 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if(htim->Instance == UWB_TIMTER) {
        HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
        dwt_forcetrxoff();
        BPhero_Distance_Measure_Specail_TAG();
    }
}
#endif

int tx_main(void)
{
#ifdef LCD_ENABLE
    OLED_ShowString(0,0,"   51UWB Node");
    sprintf(dist_str, "    Tx Node");
    OLED_ShowString(0,2,dist_str);
    OLED_ShowString(0,6,"  www.51uwb.cn");
#endif
    UWB_TIM_Init();
    HAL_TIM_Base_Start_IT(&UWB_htim);
    KalMan_Init();
    while(1)
    {
    }
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif

/******************* (C) COPYRIGHT 2021 www.51uwb.cn *****END OF FILE****/
